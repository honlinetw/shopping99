<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $url = env('MODE', 'DEVELOPMENT') === 'PRODUCTION' ? config('apiUrl.PRODUCTION')  : 'http://127.0.0.1:8000';
    return view('calculate', ['url' => $url]);
});
