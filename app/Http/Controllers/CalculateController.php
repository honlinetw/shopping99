<?php

namespace App\Http\Controllers;

use App\Http\Service\CalculateService;
use Illuminate\Http\Request;
use App\Http\Service\ValidateService;
use App\Models\CalculateRecord;

class CalculateController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [
            'result' => 'success',
            'message' => '',
            'data' => CalculateRecord::all()
        ];
        return response()->json($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 驗證傳入參數
        $rules = [
            "currency" => "required|in:USD,JPY,TWD",
            "price" => "required|numeric",
            "discount" => "required|numeric",
        ];
        $valid = (new ValidateService)->validateParam($request, $rules);
        if (!$valid['status']) return response()->json($valid);

        $post = $request->all();

        $rate = 1;
        // 計算
        $calcService = new CalculateService($post['price']);
        // 取得匯率
        $rate = $calcService->getRate($post['currency']);
        if ($post['currency'] === 'TWD') $post['price'] = $post['price'] - $post['discount'];
        // 計算結果
        $result = $post['price'] * $rate;
        // 新增結果
        $create = [
            'currency' => $post['currency'],
            'rate' => $rate,
            'price' => $post['price'],
            'discount' => $post['discount'],
            'result' => $result,
            'record_time' => date('Y-m-d H:i:s')
        ];
        CalculateRecord::insert($create);
        // 回傳
        $response = [
            "currency" => $create['currency'],
            "rate" => $create['rate'],
            "price" => $create['price'],
            "discount" => $create['discount'],
            "result" => $create['result'],
            "result_price" => $create['result']
        ];

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
