<?php

namespace App\Http\Service;


class CalculateService
{

    public $globalRate;
    public $rate = 1;

    public function curlGlobalRate()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://tw.rter.info/capi.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $this->globalRate = json_decode($response, true);
    }

    // 取得匯率
    public function getRate($currency)
    {
        if ($currency != 'TWD') $this->curlGlobalRate();

        switch ($currency) {
            case 'JPY':
                $rate = round(((1 / $this->globalRate['USDJPY']['Exrate']) * $this->globalRate['USDTWD']['Exrate']), 2);
                break;
            case 'USD':
                $rate = $this->globalRate['USDTWD']['Exrate'];
                break;

            default:
                $rate = 1;
                break;
        }

        return $rate;
    }
}
