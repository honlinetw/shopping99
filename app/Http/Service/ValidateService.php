<?php

namespace App\Http\Service;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ValidateService
{
    const VALID_BY = "VALIDATOR";

    /**
     * 驗證參數
     *
     * @param Array $data
     * @param Array $rules
     * @param Array $messages
     *
     */
    public function validateParam(Request $request, $rules, $messages = [])
    {
        $status = 1;
        $errors = [];

        switch (self::VALID_BY) {
            case 'REQUEST':
                try {
                    $request->validate($rules);
                } catch (ValidationException $exception) {
                    $status = 0;
                    $errors = $exception->validator->errors()->get('*');
                }

                break;
            case 'VALIDATOR':
                $valid = Validator::make($request->all(), $rules);
                if ($valid->fails()) {
                    $status = 0;
                    $errors = $valid->errors()->get('*');
                }

                break;

            default:

                break;
        }

        return [
            'status' => $status,
            'errors' => $errors
        ];
    }
}
