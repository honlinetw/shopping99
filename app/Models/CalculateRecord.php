<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalculateRecord extends Model
{
    use HasFactory;

    public $table = 'calculate_record';

    public $fillable = [''];

    public $timestamps = false;
}
